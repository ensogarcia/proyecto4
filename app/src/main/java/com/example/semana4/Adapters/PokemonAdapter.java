package com.example.semana4.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.example.semana4.Models.Pokemon;
import com.example.semana4.R;

import java.util.ArrayList;

public class PokemonAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Pokemon> pokemonArrayList;

    @Override
    public int getCount() {
        return pokemonArrayList.size();
    }

    @Override
    public Pokemon getItem(int position) {
        return pokemonArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView==null){
            LayoutInflater inflater=
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.grid_item, parent, false);
        }

        TextView textView = convertView.findViewById(R.id.textView);
        ImageView imageView=convertView.findViewById(R.id.imageView);
        Pokemon pkm = getItem(position);
        textView.setText(pkm.getName());

        Glide.with(context)
                .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
                    +pkm.getNumber()
                    +".png")
                .transition((DrawableTransitionOptions.withCrossFade()))
                .into(imageView);

        return convertView;
    }

    public PokemonAdapter(Context context) {
        this.context=context;
        this.pokemonArrayList= new ArrayList<>();
    }

    public void Add(ArrayList<Pokemon> pokemons){
        pokemonArrayList.addAll(pokemons);
    }
}
