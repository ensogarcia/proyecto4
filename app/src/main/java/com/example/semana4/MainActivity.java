package com.example.semana4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TabHost;
import android.widget.Toast;

import com.example.semana4.Adapters.PokemonAdapter;
import com.example.semana4.Models.Pokemon;
import com.example.semana4.Models.PokemonResponse;
import com.example.semana4.pokeapi.PokeApiService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "POKEDEX";

    private Retrofit retrofit;
    private GridView gridView;

    private PokemonAdapter pokemonAdapter;
    private boolean canLoad;
    private int offset;

    private ArrayList ImageList;
    public ArrayList NameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TabHost tabHost = findViewById(R.id.tabHost);
        tabHost.setup();


        TabHost.TabSpec spec;
        spec = tabHost.newTabSpec("One");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Tab One"); // nombre que aparece en la pestaña
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("Two");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Two");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("Three");
        spec.setContent(R.id.tab3);
        spec.setIndicator("Three");
        tabHost.addTab(spec);

        NameList = new ArrayList<String>();
        ImageList = new ArrayList<Integer>();

        retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        gridView = findViewById(R.id.gridView);
        pokemonAdapter = new PokemonAdapter(getApplicationContext());
        gridView.setAdapter(pokemonAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pokemon pkm = pokemonAdapter.getItem(position);
                Toast.makeText(getApplicationContext(), pkm.getName(), Toast.LENGTH_LONG).show();


                NameList.add(pkm.getName());
            }
        });

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }


            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                    if (canLoad) {
                        canLoad = false;

                        GetPokemons(offset);
                        offset += 20;
                        Toast.makeText(getApplicationContext(), "load more pokemons", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        offset = 0;
        canLoad = true;

//        GetPokemons();
    }

    private void GetPokemons(int offset) {
        PokeApiService pokeApiService = retrofit.create(PokeApiService.class);
        Call<PokemonResponse> pokemonResponseCall = pokeApiService.GetPokemonList(20, offset);

        pokemonResponseCall.enqueue(new Callback<PokemonResponse>() {
            @Override
            public void onResponse(Call<PokemonResponse> call, Response<PokemonResponse> response) {
                if (response.isSuccessful()){
                    PokemonResponse pokemonResponse = response.body();
                    ArrayList<Pokemon> pokemons = pokemonResponse.getResults();

                    for (int i = 0; i < pokemons.size(); i++) {
                        Pokemon p = pokemons.get(i);
                        Log.i(TAG, "Pokemon: " + p.getName());
                    }

                    pokemonAdapter.Add(pokemons);
                    pokemonAdapter.notifyDataSetChanged();
                    canLoad = true;
                }
                else {
                    Log.e(TAG, "onResponse -> " + response.errorBody());
                    canLoad = true;
                }
            }

            @Override
            public void onFailure(Call<PokemonResponse> call, Throwable t) {
                Log.e(TAG, "onFailure -> " + t.getMessage());
            }
        });
    }
}
