package com.example.semana4.pokeapi;

import com.example.semana4.Models.PokemonResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PokeApiService {
    @GET("pokemon")
    Call<PokemonResponse> GetPokemonList(@Query("limit") int limit, @Query("offset") int offset);
}
