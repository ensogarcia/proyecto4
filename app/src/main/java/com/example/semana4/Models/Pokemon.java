package com.example.semana4.Models;

public class Pokemon {
    private String name;
    private String url;

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }

    public String getUrl(){
        return url;
    }
    public void setUrl(String url){
        this.url=url;
    }

    public int getNumber(){
        String[] urlParts=getUrl().split("/");
        return Integer.parseInt(urlParts[urlParts.length-1]);
    }
}
